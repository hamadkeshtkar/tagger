import React from "react";

const Frame = props => {
  return (
    <div id={"c" + props.id} className={"content " + props.className}>
      {props.children}
    </div>
  );
};

export default Frame;
