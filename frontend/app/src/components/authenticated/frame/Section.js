import React from "react";

const Section = props => {
  return (
    <div
      className="section"
    >
      <header>{props.title} <i>{props.hint}</i></header>
      <div className="list" style={{ flexDirection: props.direction }}>
        {props.children}
      </div>
    </div>
  );
};

export default Section;
