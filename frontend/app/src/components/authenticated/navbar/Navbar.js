import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faHome } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
import "../../../assets/style/authenticated/nav.scss";

const NavBar = props => {
  return <div className="nav">{props.children}</div>;
};

export default NavBar;
