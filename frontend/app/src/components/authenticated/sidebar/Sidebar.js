import React from "react";

import "../../../assets/style/authenticated/side.scss";

const SideBar = props => {
  return (
    <div className="side">
      <h1 className="header">{props.header}</h1>
      <p className="paragraph">{props.text}</p>
      <div className="crawls">
        <header>My Crawls</header>
        <div className="list">
          {props.children}
        </div>
      </div>
    </div>
  );
};

export default SideBar