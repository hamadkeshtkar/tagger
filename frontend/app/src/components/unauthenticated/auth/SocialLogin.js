import React from 'react'
import { Button } from "react-materialize";

import { faGoogle, faFacebook } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const SocialLogin = () => {
  return (
    <div className="social-login">
      <Button node="button" type="submit" waves="light" className="google">
        <FontAwesomeIcon icon={faGoogle} />
      </Button>
      <Button node="button" type="submit" waves="light" className="facebook">
        <FontAwesomeIcon icon={faFacebook} />
      </Button>
    </div>
  );
};

export default SocialLogin;