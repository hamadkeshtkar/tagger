import React from 'react'

const Separator = ({ text }) => {
  return <div className="separator">{text}</div>;
};

export default Separator;