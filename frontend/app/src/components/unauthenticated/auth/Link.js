import React from 'react'

const LinkWrapper = props => {
  return <div className="others">{props.children}</div>;
};

const Link = (props) => {
  return <p>{props.children}</p>;
};

export { LinkWrapper, Link }