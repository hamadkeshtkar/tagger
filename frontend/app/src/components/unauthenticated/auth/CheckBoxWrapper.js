import React from "react";

const CheckBoxWrapper = props => {
  return <div className="checkbox">{props.children}</div>;
};

export default CheckBoxWrapper;
