import React from 'react';

export default class CrawlItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: props.title,
            isActive: props.isActive,
            isSelected: props.isSelected,
        }
    }

    selectMe = () => this.setState({ isSelected: true });

    render() {
        var cssStyleClasses = this.state.isSelected ? ' selected ' : '';
        cssStyleClasses += this.state.isActive ? '' : ' disabled ';

        return (<div className={'item ' + cssStyleClasses} onClick={ this.selectMe }>
            <h2>{this.state.title}</h2>
        </div>)
    }
}