export const CrawlStateReducer = (state, action) => {
  switch (action.type) {
    case "CRAWL1":
      state.CRAWL1 = action.value;
      localStorage.setItem("CRAWL_STATES", state);
      return state;
    case "CRAWL2":
      state.CRAWL2 = action.value;
      localStorage.setItem("CRAWL_STATES", state);
      return state;
    case "CRAWL3":
      state.CRAWL3 = action.value;
      localStorage.setItem("CRAWL_STATES", state);
      return state;
    default:
      return state;
  }
};
