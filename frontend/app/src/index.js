import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import { AuthProvider } from "./contexts/AuthContext";

ReactDOM.render(
  <div className="app">
    <AuthProvider>
      <App />
    </AuthProvider>
  </div>,
  document.getElementById("root")
);
