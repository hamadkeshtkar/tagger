import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import HomeFrame from "./authenticated/Home";
import CrawlFrame from "./authenticated/CrawlFrame";

import SideBar from "./authenticated/SideBar";

import NavBarView from "./authenticated/NavBar";
import SettingsView from "./authenticated/Settings";
import { useAuth } from "../contexts/AuthContext";
import { CrawlStateProvider } from "../contexts/CrawlStateContext";

import "../assets/style/authenticated/auth.scss";
import "../assets/style/authenticated/main.scss";
import { Preloader } from "react-materialize";
import useAxios from "@use-hooks/axios";

const AuthenticatedRoutes = () => {
  const { authDispatch } = useAuth();
  const { response, error, loading, reFetch } = useAxios({
    url: "http://127.0.0.1:8000/rest-auth/login/",
    method: "POST",
    options: {
      data: {},
      headers: {
        "Content-type": "application/json"
      }
    }
  });

  // if (localStorage.getItem("first_login")) {
  //   localStorage.setItem("first_login", false);
  //   return (
  //     <div
  //       className="auth-wrapper"
  //       style={{
  //         display: "flex",
  //         justifyContent: "center",
  //         alignItems: "center"
  //       }}
  //     >
  //       <Preloader active size="big" color="blue" flashing />
  //     </div>
  //   );
  // }
  return (
    <CrawlStateProvider>
      <div className="auth-wrapper">
        <div className="grid">
          <div className="main">
            <Switch>
              <Route exact path="/" component={HomeFrame} />
              <Route path="/crawl/:crawl_id" component={CrawlFrame} />
              <Route path="/settings" component={SettingsView} />
              <Route path="/logout">
                {() => {
                  authDispatch({ type: "LOGOUT" });
                  return (
                    <Redirect
                      to={{
                        pathname: "/"
                      }}
                    />
                  );
                }}
              </Route>
              <Route path="*">
                <Redirect
                  to={{
                    pathname: "/"
                  }}
                />
              </Route>
            </Switch>
          </div>
          <SideBar />
          <NavBarView />
        </div>
      </div>
    </CrawlStateProvider>
  );
};

export default AuthenticatedRoutes;
