import React from "react";
import {
  Frame,
  Header,
  LinkWrapper,
  Link as LinkText
} from "../../components/unauthenticated/auth/Components";
import { Link } from "react-router-dom";

const ForgetPasswordView = () => {
  return (
    <Frame>
      <Header text="Forget Password Page" />
      <LinkWrapper>
        <LinkText>
          Login? <Link to="/login">click here</Link>
        </LinkText>
      </LinkWrapper>
    </Frame>
  );
};

export default ForgetPasswordView;
