import React from "react";
import { Link } from "react-router-dom";

const HomeView = () => {
  return (
    <div>
      <h1>Hello Home</h1>
      <Link to="/login">Login</Link>
    </div>
  );
};

export default HomeView;
