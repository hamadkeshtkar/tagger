import React, { useState } from "react";
import { TextInput, Button, Checkbox, Preloader, Col } from "react-materialize";

import useAxios from "@use-hooks/axios";

import { Link, Redirect } from "react-router-dom";

import {
  Frame,
  Header,
  CheckBoxWrapper,
  SocialLogin,
  Separator,
  LinkWrapper,
  Link as LinkText
} from "../../components/unauthenticated/auth/Components";

import { useAuth } from "../../contexts/AuthContext";

const LoginView = () => {
  const { authDispatch } = useAuth();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { response, error, loading, reFetch } = useAxios({
    url: "http://127.0.0.1:8000/rest-auth/login/",
    method: "POST",
    options: {
      data: {
        username: username,
        password: password
      },
      headers: {
        "Content-type": "application/json"
      }
    }
  });

  const handleSubmit = e => {
    e.preventDefault();
    reFetch();
  };

  const form = () => {
    return (
      <form action="" method="post" onSubmit={handleSubmit}>
        <TextInput
          username
          label="Username"
          required
          validate
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
        <TextInput
          label="Password"
          required
          password
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <CheckBoxWrapper>
          <Checkbox
            checked
            filledIn
            id="checkbox_1"
            label="Remember Me"
            value="rememberme"
          />
        </CheckBoxWrapper>
        <Button node="button" type="submit" waves="light">
          Login
        </Button>
      </form>
    );
  };
  if (loading)
    return (
      <Frame>
        <Col s={12}>
          <Preloader active color="blue" flashing size="big" />
        </Col>
      </Frame>
    );
  if (response) {
    localStorage.setItem("first_login", true);
    localStorage.setItem("CRAWL_STATES", {
      CRAWL1: false,
      CRAWL2: true,
      CRAWL3: false
    });
    authDispatch({ type: "LOGIN", token: response.data.key });
  }
  return (
    <Frame>
      <Header text="Welcome Back" />
      <SocialLogin />
      <Separator text="or login with email" />
      <p style={{ color: "red" }}>
        {error ? "Your Username or password is incorrect!" : ""}
      </p>
      {form()}
      <LinkWrapper>
        <LinkText>
          didnt register yet? <Link to="/register">click here</Link>
        </LinkText>
        <LinkText>
          Forget your Password? <Link to="/forget">click here</Link>
        </LinkText>
      </LinkWrapper>
    </Frame>
  );
};

export default LoginView;
