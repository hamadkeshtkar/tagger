import React from "react";
import {
  Frame,
  Header,
  LinkWrapper,
  Link as LinkText
} from "../../components/unauthenticated/auth/Components";
import { Link } from "react-router-dom";

const RegisterView = () => {
  return (
    <Frame>
      <Header text="Hey there" />
      <LinkWrapper>
        <LinkText>
          didnt register yet? <Link to="/login">click here</Link>
        </LinkText>
      </LinkWrapper>
    </Frame>
  );
};

export default RegisterView;
