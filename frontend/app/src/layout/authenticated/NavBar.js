import React from "react";
import NavBar from "../../components/authenticated/navbar/Navbar";
import { NavLink } from "react-router-dom";
import {
  faHome,
  faCog,
  faSignOutAlt
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const NavBarView = () => {
  return (
    <NavBar>
      <NavLink exact to="/" className="item">
        <FontAwesomeIcon icon={faHome} />
      </NavLink>
      <NavLink to="/settings" className="item">
        <FontAwesomeIcon icon={faCog} />
      </NavLink>
      <NavLink to="/logout" className="item">
        <FontAwesomeIcon icon={faSignOutAlt} />
      </NavLink>
    </NavBar>
  );
};

export default NavBarView;
