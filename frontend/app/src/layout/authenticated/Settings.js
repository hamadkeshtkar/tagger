import React from "react";
import { Frame, Header } from "../../components/authenticated/frame/Components";

const SettingsView = props => {
  return (
    <Frame>
      <Header>Settings</Header>
    </Frame>
  );
};

export default SettingsView;
