import React from "react";
import { Frame, Header, Description, Section } from "../../components/authenticated/frame/Components";
import { Redirect, useParams } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { Preloader, Checkbox, Switch, Chip, Icon } from "react-materialize";

import { Autocomplete, autocompleteOptions } from "materialize-css";

const CrawlFrame = props => {
  const { crawl_id } = useParams();

  if (crawl_id > 3 || crawl_id < 1)
    return <Redirect to="/" />;

  // return (
  //   <Frame className="flex-center">
  //     <Preloader
  //       active
  //       size="big"
  //       color="blue"
  //       flashing
  //     />
  //   </Frame>
  // )

  return (
    <Frame id={crawl_id}>
      <Header>Crawl {crawl_id}</Header>
      <Section title="Tags" direction='column' hint="max: 10 tags">
        <Chip
          close={false}
          closeIcon={<Icon className="close">close</Icon>}
          options={{
            placeholder: "tag",
            secondaryPlaceholder: "+ tag",
            autocompleteOptions: {
              data: {
                Apple: null,
                Google: null,
                Microsoft: null
              },
              minLength: 2,
              onAutocomplete: function noRefCheck() { }
            },
            limit: 10,
          }}
        />
      </Section>
      <Section title="Websites" direction="row" hint="max: 10 websites">
        <Checkbox id="test" label="TEst" ></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
        <Checkbox id="test2" label="TEst2"></Checkbox>
      </Section>
      <Section title="Settings" direction="column">
        <Switch
          id="Switch-10"
          offLabel="Enable The Crawl"
          onChange={function noRefCheck() { }}
        />
        <Switch
          id="Switch-12"
          offLabel="Notify me with email"
          onChange={function noRefCheck() { }}
        />
        <Switch
          disabled
          id="Switch-11"
          offLabel="Notify me with SMS"
          onChange={function noRefCheck() { }}
        />
      </Section>
      <button id={"c" + crawl_id} className="float-btn">Save <FontAwesomeIcon icon={faSave} /></button>
    </Frame>
  );
};

export default CrawlFrame;
