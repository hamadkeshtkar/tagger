import React from "react";

import Frame from "../../components/main/frame";

import "../assets/style/main.scss";

export default class Main extends React.Component {
    render() {
        return (
            <div className="main">
                <Frame />
            </div>
        );
    }
}