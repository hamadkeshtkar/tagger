import React from "react";
import { NavLink } from "react-router-dom";

import SideBar from "../../components/authenticated/sidebar/Sidebar";

import "../../assets/style/authenticated/side.scss";
import { useCrawlState } from "../../contexts/CrawlStateContext";

const SideBarView = props => {
  const { crawlState } = useCrawlState();
  return (
    <SideBar header="Hey Hamad" text="Welcome Back">
      <NavLink
        to="/crawl/1"
        className={crawlState.CRAWL1 ? "item" : "item disabled"}
      >
        <h2>1</h2>
      </NavLink>
      <NavLink
        to="/crawl/2"
        className={crawlState.CRAWL2 ? "item" : "item disabled"}
      >
        <h2>2</h2>
      </NavLink>
      <NavLink
        to="/crawl/3"
        className={crawlState.CRAWL3 ? "item" : "item disabled"}
      >
        <h2>3</h2>
      </NavLink>
    </SideBar>
  );
};

export default SideBarView;
