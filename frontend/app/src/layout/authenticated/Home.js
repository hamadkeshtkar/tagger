import React from "react";
import {Frame, Header, Section} from "../../components/authenticated/frame/Components";
import { Link } from "react-router-dom";

const HomeView = () => {
  return <Frame>
    <Header>Home</Header>
    <Section title="Last Notifies" direction="column">
      <Link className="link">[C1][5:16 PM, 26/03] One Piece in TakAnime</Link>
      <Link className="link">[C1][5:16 PM, 26/03] One Piece in TakAnime</Link>
      <Link className="link">[C1][5:16 PM, 26/03] One Piece in TakAnime</Link>
      <Link className="link">[C1][5:16 PM, 26/03] One Piece in TakAnime</Link>
    </Section>
  </Frame>;
};

export default HomeView;
