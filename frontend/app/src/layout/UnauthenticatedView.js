import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import LoginView from "./unauthenticated/Login";
import RegisterView from "./unauthenticated/Register";
import ForgetPasswordView from "./unauthenticated/ForgetPassword";
import HomeView from "./unauthenticated/Home";

import "../assets/style/unauthenticated/unauth.scss";

const UnAuthenticatedRoutes = () => {
  return (
    <Switch>
      <Route exact path="/" component={HomeView} />
      <div className="unauth-wrapper">
        <Route path="/login" component={LoginView} />
        <Route path="/register" component={RegisterView} />
        <Route path="/forget" component={ForgetPasswordView} />
      </div>
      <Route path="*">
        <Redirect
          to={{
            pathname: "/"
          }}
        />
      </Route>
    </Switch>
  );
};

export default UnAuthenticatedRoutes;
