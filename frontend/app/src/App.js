import React from "react";

import { BrowserRouter } from "react-router-dom";

import AuthenticatedRoutes from "./layout/AuthenticatedView";
import UnAuthenticatedRoutes from "./layout/UnauthenticatedView";

import { useAuth } from "./contexts/AuthContext";

import "materialize-css/sass/materialize.scss";
import "./assets/style/app.scss";

const App = () => {
  const { token } = useAuth();
  return (
    <BrowserRouter>
      {token ? <AuthenticatedRoutes /> : <UnAuthenticatedRoutes />}
    </BrowserRouter>
  );
};

export default App;
