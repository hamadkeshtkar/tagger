import React, { createContext, useReducer, useContext } from "react";
import { CrawlStateReducer } from "../reducers/CrawlStateReducer";

const CrawlStateContext = createContext();

const CrawlStateProvider = props => {
  const [crawlState, crawlStateDispatch] = useReducer(
    CrawlStateReducer,
    localStorage.getItem("CRAWL_STATES")
  );

  return (
    <CrawlStateContext.Provider
      value={{ crawlState, crawlStateDispatch }}
      {...props}
    >
      {props.children}
    </CrawlStateContext.Provider>
  );
};

const useCrawlState = () => useContext(CrawlStateContext);

export { CrawlStateProvider, useCrawlState };
