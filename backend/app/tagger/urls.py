from django.contrib import admin
from django.urls import path, include
# from rest_framework.documentation import include_docs_urls

# from scrapers.views import CrawlCreateAPIView, CrawlRUDAPIView
# from tags.views import TagListCreateAPIView

from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('rest-auth/', include('rest_auth.urls')),
    path('graphql/', GraphQLView.as_view(graphiql=True)),
]
