import graphene
import graphql_jwt

from graphene_django.types import DjangoObjectType

from tags.models import Tag
from scrapers.schema import CrawlQuery, WebsiteQuery, CreateCrawl


class Mutation(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    create_crawl = CreateCrawl.Field()


class Query(WebsiteQuery, CrawlQuery, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
