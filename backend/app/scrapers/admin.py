from django.contrib import admin
from .models import Crawl, Website


admin.site.register(Crawl)
admin.site.register(Website)
