from django.contrib.auth import get_user_model
from rest_framework import serializers

from .models import Crawl, Tag, Website

User = get_user_model()

class CrawlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Crawl
        fields = '__all__'

# class CrawlSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     tags = serializers.CharField(required=True, max_length=100)
#     website = serializers.CharField(required=True)
#     active = serializers.BooleanField(required=True)
#     notify = serializers.BooleanField(required=True)
    
#     def create(self, validated_data):
#         Tag.objects.bulk_create([
#             Tag(name=tag) for tag in validated_data['tags']
#         ], ignore_conflicts=True)
        
#         crawl = Crawl.objects.create(
#             active=validated_data['active'],
#             notify=validated_data['notify'],
#         )
        
#         crawl.website.add(*validated_data['website'])
#         crawl.tag.add(*Tag.objects.in_bulk(validated_data['tags'], field_name='name').values())
        
#         return crawl
    
#     def update(self, instance, validated_data):
#         pass
    
#     def validate_tags(self, value):
#         value = value.split(',')
        
#         if len(value) == 0:
#             raise serializers.ValidationError("Select At least a one Tag")
                
#         return value
    
#     def validate_website(self, value):
#         value = Website.objects.in_bulk(value.split(','), field_name='name')
        
#         if len(value) == 0:
#             raise serializers.ValidationError("Select At least a one Website")
        
#         return value.values()
    