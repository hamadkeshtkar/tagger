# Generated by Django 2.2.11 on 2020-03-19 12:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0001_initial'),
        ('scrapers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='crawl',
            name='email',
            field=models.BooleanField(default=False),
        ),
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=250)),
                ('link', models.CharField(max_length=500)),
                ('added_on', models.DateTimeField(auto_now=True)),
                ('tag', models.ManyToManyField(to='tags.Tag')),
            ],
        ),
    ]
