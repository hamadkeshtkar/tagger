# Generated by Django 2.2.11 on 2020-04-03 12:00

from django.db import migrations
import django_extensions.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('scrapers', '0008_auto_20200403_1159'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='slug',
            field=django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='name'),
        ),
    ]
