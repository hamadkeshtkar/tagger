import graphene
from graphene_django.types import DjangoObjectType

from django.contrib.auth import get_user_model

from tagger.settings import MAX_TAGS_PER_CRAWL, MAX_WEBSITES_PER_CRAWL

from tags.schema import TagInput
from tags.models import Tag

from .models import Crawl, Website

User = get_user_model()


class UserType(DjangoObjectType):
    class Meta:
        model = User


class WebsiteType(DjangoObjectType):
    class Meta:
        model = Website


class CrawlType(DjangoObjectType):
    class Meta:
        model = Crawl
        fields = '__all__'


class WebsiteInput(graphene.InputObjectType):
    slug = graphene.String()


class CrawlInput(graphene.InputObjectType):
    active = graphene.Boolean()
    notify = graphene.Boolean()
    tags = graphene.List(TagInput)
    websites = graphene.List(WebsiteInput)


class CreateCrawl(graphene.Mutation):
    class Arguments:
        input = CrawlInput(required=True)

    create_crawl = graphene.Field(CrawlType)

    def mutate(self, info, input):
        Tag.objects.bulk_create([
            Tag(name=tag.name) for tag in input.tags
        ], ignore_conflicts=True)

        crawl = Crawl.objects.create(
            active=input.active,
            notify=input.notify,
            user=info.context.user,
        )
        crawl.tag.add(
            *Tag.objects.in_bulk([tag.name for tag in input.tags[:MAX_TAGS_PER_CRAWL]], field_name='name').values())

        crawl.website.add(
            *Website.objects.in_bulk([website.slug for website in input.websites[:MAX_WEBSITES_PER_CRAWL]], field_name='slug').values())

        crawl.save()

        return CreateCrawl(create_crawl=crawl)


class CrawlQuery(object):
    user_crawls = graphene.List(CrawlType)

    def resolve_user_crawls(self, info, **kwargs):
        return Crawl.objects.filter(user=info.context.user)


class WebsiteQuery(object):
    all_active_websites = graphene.List(WebsiteType)

    def resolve_all_active_websites(self, info, **kwargs):
        return Website.objects.filter(active=True)
