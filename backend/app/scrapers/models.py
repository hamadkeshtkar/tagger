from django.db import models
from django.contrib.auth import get_user_model
from django_extensions.db.fields import AutoSlugField

from tags.models import Tag

User = get_user_model()

class Website(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = AutoSlugField(populate_from='name', unique=True)
    url = models.CharField(max_length=100)
    added = models.DateTimeField(auto_now=True)
    updated = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name


class Crawl(models.Model):
    tag = models.ManyToManyField(Tag)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    website = models.ManyToManyField(Website)
    notify = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ['created_on']
    
class Data(models.Model):
    title = models.CharField(max_length=250)
    link = models.CharField(max_length=500)
    tag = models.ManyToManyField(Tag)
    added_on = models.DateTimeField(auto_now=True)