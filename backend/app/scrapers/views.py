from django.shortcuts import render

from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from .serializers import CrawlSerializer
from .models import Crawl

class CrawlCreateAPIView(ListCreateAPIView):
    queryset = Crawl.objects.all()
    serializer_class = CrawlSerializer
    
class CrawlRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = CrawlSerializer
    model = Crawl