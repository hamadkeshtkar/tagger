from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView

from .models import Tag
from .serializers import TagSerializer

class TagListCreateAPIView(ListCreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer