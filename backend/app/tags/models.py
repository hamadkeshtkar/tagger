from django.db import models
from django_extensions.db.fields import AutoSlugField

class Tag(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = AutoSlugField(populate_from='name')
    
    created_on = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.name